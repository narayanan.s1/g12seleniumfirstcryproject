package CalledMethods;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class AddressPage {

	
	WebDriver driver;

	// Actions act = new Actions(driver);
	public AddressPage(WebDriver driver) {
		this.driver = driver;
}
	@FindBy(xpath="//div[@class='M14_white add_deliveryaddr']")
	private WebElement addaddressbtn;
	public void addbtn()
	{
	    //Actions act = new Actions(driver);
	   // act.doubleClick(addtocartbtn).perform();
		addaddressbtn.click();
	    
	}
	@FindBy(xpath="//label[text()='Full Name*']")
	private WebElement addnamebtn;
	public void addnamtxt()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;

		js.executeScript("arguments[0].click()",addnamebtn);
	    addnamebtn.click();
		addnamebtn.sendKeys("Narayanan S");
	    
	}
@FindBy(name="ShipMobileNo")

private WebElement addmobilebtn;
public void addmobtxt()
{
    //Actions act = new Actions(driver);
   // act.doubleClick(addtocartbtn).perform();
	addmobilebtn.sendKeys("9876543210");
    
}

@FindBy(name="ShipPinCode")
private WebElement addpincodebtn;
public void addpinctxt()
{
    //Actions act = new Actions(driver);
   // act.doubleClick(addtocartbtn).perform();
	addpincodebtn.sendKeys("600122");
    
}
@FindBy(name="AddressLine1")
private WebElement addadd1btn;
public void addadd1txt()
{
    //Actions act = new Actions(driver);
   // act.doubleClick(addtocartbtn).perform();
	addadd1btn.sendKeys("3,west mada street");
    
}
@FindBy(name="AddressLine2")
private WebElement addadd2btn;
public void addadd2txt()
{
    //Actions act = new Actions(driver);
   // act.doubleClick(addtocartbtn).perform();
	addadd2btn.sendKeys("Chennai");   
}

@FindBy(name="AddressLine3")
private WebElement addadd3btn;
public void addadd3txt()
{
    //Actions act = new Actions(driver);
   // act.doubleClick(addtocartbtn).perform();
	addadd3btn.sendKeys("Kovur EB");   
}
@FindBy(xpath="//div[text()=' SAVE ADDRESS']")
private WebElement addsaveaddressbtn;
public void addsavadd()
{
    //Actions act = new Actions(driver);
   // act.doubleClick(addtocartbtn).perform();
	addsaveaddressbtn.click();   
}









	
	
	
	
	
	
	
	
	
}
